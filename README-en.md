# Webhook Receiver Compose

An example usage of [Webhook Receiver](https://gitlab.com/wp-id/utils/webhook-receiver/) with [docker compose](https://docs.docker.com/compose/).

## Setup
1. Clone this repo
    ```sh
    git clone https://gitlab.com/wp-id/docker/webhook-receiver-compose
    ```
1. Create a `.env` file from the sample file
    ```sh
    cp .env.sample .env
    ```
1. Create config from the provided sample and edit it
    ```sh
    cp config/config.sample.json config/config.json
    ```
1. Copy _one of_ the sample override files:
    * With [proxy](https://gitlab.com/wp-id/docker/proxy): `cp docker-compose.override-proxy.yml docker-compose.override.yml`
    * Without proxy: `cp docker-compose.override-single.yml docker-compose.override.yml`
1. Add `127.0.0.1 whr.local` to `/etc/hosts`
1. Bring the containers up
    ```sh
    docker-compose up
    ```
