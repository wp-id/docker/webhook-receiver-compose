# Webhook Receiver Compose

View readme in [English](README-en.md).

Contoh penggunaan [Webhook Receiver](https://gitlab.com/wp-id/utils/webhook-receiver/) dengan [docker compose](https://docs.docker.com/compose/).

## Persiapan
1. Clone repository ini
    ```sh
    git clone https://gitlab.com/wp-id/docker/webhook-receiver-compose
    ```
1. Buat berkas `.env` dari berkas contoh lalu sesuaikan
    ```sh
    cp .env.sample .env
    ```
1. Buat pengaturan dari berkas contoh lalu sesuaikan
    ```sh
    cp config/config.sample.json config/config.json
    ```
1. Salin _salah satu_ dari berkas override:
    * Dengan [proxy](https://gitlab.com/wp-id/docker/proxy): `cp docker-compose.override-proxy.yml docker-compose.override.yml`
    * Tanpa proxy: `cp docker-compose.override-single.yml docker-compose.override.yml`
1. Tambahkan `127.0.0.1 whr.local` to `/etc/hosts`
1. Jalankan container
    ```sh
    docker-compose up
    ```
